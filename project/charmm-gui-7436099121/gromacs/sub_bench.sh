#!/bin/bash -x
#SBATCH --nodes=4
#SBATCH --exclusive
#-SBATCH -p general_requeue
#-SBATCH -p general
#SBATCH --reservation=class_eric_may 
#SBATCH -t 00:10:00

module purge

export MODULEPATH=/shared/maylab/mayapps/mod:$MODULEPATH
source /shared/maylab/scripts/bash/modload.sh
modload gromacs/2019/haswell-mpi

indir="."
outdir="."

nnodes=$SLURM_JOB_NUM_NODES
cpn=$SLURM_CPUS_ON_NODE

mkdir -p $outdir



gmx_mpi grompp \
	-f $indir/step5_production.mdp \
	-o $outdir/step5_prod.tpr \
	-c $outdir/step4.1_equilibration.gro \
	-n $indir/index.ndx \
	-p $indir/topol.top \
	-r $indir/step4.1_equilibration.gro

nsteps=15000
n=1
while [[ $n -le $nnodes ]]
do
    let nt=$n*$cpn
    mpirun -np $nt \
	   gmx_mpi mdrun \
	   -nsteps $nsteps \
	   -s $outdir/step5_prod.tpr \
	   -g $outdir/$nt.log
    let n++
done

exit

