#!/bin/bash
#SBATCH --reservation class_eric_may
#SBATCH -J prot_eq
#SBATCH -o %j.out
#SBATCH --nodes=1
#SBATCH -n 24
#SBATCH -p general
#SBATCH --exclude=cn[65-69,71-136,325-343,345-353,355-358,360-364,369-398,400-401],gpu[07-10]
#SBATCH -t 12:00:00

module purge

export MODULEPATH=/shared/maylab/mayapps/mod:$MODULEPATH
source /shared/maylab/scripts/bash/modload.sh
modload gromacs/2019/haswell-basic

# files
pdb=step3_charmm2gmx.pdb
top=topol.top
ndx=index.ndx
script=CGUI_protein_eq.sh

# Minimization >> step4.0
if [ ! -f step4.0_minimization.tpr ]; then
    gmx grompp \
	-f step4.0_minimization.mdp \
	-o step4.0_minimization.tpr \
	-c $pdb \
	-r $pdb \
	-p $top
    gmx mdrun \
	-v -deffnm step4.0_minimization
fi

# Equilibration >> step4.1
if [ -f step4.0_minimization.gro ] &&
       [ ! -f step4.1_equilibration.tpr ]; then
    gmx grompp \
	-f step4.1_equilibration.mdp \
	-o step4.1_equilibration.tpr \
	-c step4.0_minimization.gro \
	-r $pdb \
	-n $ndx \
	-p $top
    gmx mdrun \
	-deffnm step4.1_equilibration \
	-maxh 11.5
fi

if [ -f step4.1_equilibration.trr ] &&
       [ ! -f step4.1_equilibration.gro ]; then
    gmx mdrun \
	-deffnm step4.1_equilibration \
	-cpi step4.1_equilibration.cpt \
	-maxh 11.5
fi

if [ -f step4.1_equilibration.gro ]; then
    printf "\nEquilibration is done. Next: \n1) Open step4.1_equilibration.gro in VMD to ensure it looks okay. \n2) Fisbatch in, load gromacs, and run the following energy analyses from lysozyme tutorial to ensure proper minimization and equilibration. \n   - gmx energy -f step4.0_minimization.edr -o potential.xvg \n   - gmx energy -f step4.1_equilibration.edr -o temperature.xvg \n   - gmx energy -f step4.1_equilibration.edr -o density.xvg \n3) Then, sbatch restart.sh to start production.\n"
fi

exit

