#!/bin/bash
#SBATCH --time 12:00:00
#SBATCH --ntasks 48
#SBATCH --partition general
#-SBATCH --ntasks 4
#-SBATCH --partition debug
#SBATCH --job-name restart
#SBATCH --dependency singleton
#SBATCH --output %j.out
# Subset to haswell nodes.
# 1) Find node architecture using gcc:
#    clush -Bw @cpu:all "/apps2/gcc/9.2.0/bin/gcc -Q -march=native --help=target | grep '  -march' | awk '{print \$2}'"
# 2) Invert the set:
#    nodeset -f @cpu:all -x cn[137-320]
#SBATCH --exclude cn[65-69,71-136,325-353,355-364,369-398,400-401],gpu[03-11],gtx[01-16]

set -e

# If our result file is created, cancel all remaining jobs.
file_result=step5_production.trr
cancel_remaining() {
    if [ -f $file_result ]; then
	name=$(sacct -nX -j $SLURM_JOB_ID -o jobname)
	ids=$(sacct -nX --name=$name -o jobid)
	echo $ids | xargs -n1 scancel
	exit 0
    fi
}

module purge

export MODULEPATH=/shared/maylab/mayapps/mod:$MODULEPATH
source /shared/maylab/scripts/bash/modload.sh
modload gromacs/2019/haswell-mpi

# Files
gro=step4.1_equilibration.gro
top=topol.top
ndx=index.ndx

# Production >> step5
if [ ! -f step5_production.tpr ]; then
    echo "Creating tpr file..."
    gmx_mpi grompp \
	    -f step5_production.mdp \
	    -o step5_production.tpr \
	    -c $gro \
	    -r $gro \
	    -p $top
fi

if [ ! -f step5_production.trr ]; then
    echo "Creating trr file..."
    mpirun -np $SLURM_NTASKS \
	   gmx_mpi mdrun \
	   -deffnm step5_production \
	   -maxh 11.5
fi

# Process Trajectory for smaller download
if [ -f $file_result ] ; then
    echo "Creating xtc file..."
    echo 0 |
	gmx_mpi trjconv \
		-f $file_result \
		-s step5_production.tpr \
		-dt 100 \
		-pbc whole \
		-o production.xtc
fi

echo "Job complete!"
cancel_remaining
