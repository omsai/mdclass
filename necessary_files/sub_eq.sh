#!/bin/bash
#SBATCH --nodes 1
#SBATCH -n 24
#SBATCH --reservation class_eric_may
#SBATCH -t 1:00:00

export MODULEPATH=/shared/maylab/mayapps/mod:$MODULEPATH
source /shared/maylab/scripts/bash/modload.sh
modload gromacs/2019/haswell-basic

nt=$SLURM_NTASKS
# From the tutorial page:
gmx grompp -f nvt.mdp -c em.gro -r em.gro -p topol.top -o nvt.tpr
gmx mdrun -nt $nt -deffnm nvt
gmx grompp -f npt.mdp -c nvt.gro -r nvt.gro -t nvt.cpt -p topol.top -o npt.tpr
gmx mdrun -nt $nt -deffnm npt

# -c flag is input for the first phase.
# -r flag is for restraints reference position.
# -t flag takes a .cpt state file.  Like a restart; we don't
#    re-generate velocities at this stage.  Not a true restart because
#    we are regenerating some things compared to the previous
#    simulation.
