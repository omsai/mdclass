#!/bin/bash
#SBATCH --nodes=1
#SBATCH -n 4
#SBATCH -p general
#SBATCH --exclude=cn[65-69,71-136,325-343,345-353,355-358,360-364,369-398,400-401],gpu[07-10]
#SBATCH -t 12:00:00

source /shared/maylab/scripts/bash/modload.sh
modload gromacs/2019/haswell-basic


gmx mdrun -nt 4 -deffnm nvt
